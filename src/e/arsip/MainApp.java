/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip;

import e.arsip.config.AppConfig;
import e.arsip.util.DialogBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Deri Armanda
 */
public class MainApp extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        DialogBuilder.init(stage);
        
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Login.fxml"));
        Scene scene = new Scene(root);        
        stage.setTitle(AppConfig.APP_NAME);
        stage.getIcons().add(AppConfig.APP_LOGO);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
