/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import e.arsip.config.AppDatabase;
import e.arsip.data.model.Header;
import e.arsip.data.model.Unit;
import e.arsip.data.model.User;
import e.arsip.util.DialogBuilder;
import e.arsip.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Deri Armanda
 */
public class UserManager {
    
    private static UserManager SINGLETON;
    private final Dao<User, Integer> userDao;
    private final Dao<Header, Integer> headerDao;
    private final Dao<Unit, Integer> unitDao;
    
    private User activeUser;
    
    private UserManager() throws SQLException {
        userDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                User.class
        );
        headerDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Header.class
        );
        unitDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Unit.class
        );
        activeUser = new User();
    }
    
    public static void init() {
        try {
            SINGLETON = new UserManager();
        } catch (SQLException ex) {
            SINGLETON = null;
            System.err.println("Error while creating User DAO: "+ex);
            DialogBuilder.showError("Gagal menyambungkan ke tabel Users.");
        }
    }
    
    public static UserManager getInstance() {
        if (SINGLETON == null) init();
        return SINGLETON;
    }

    public User getActiveUser() {
        return activeUser;
    } 
    
    public void authenticate(User user, ICallback<String> callback) {
        try {
            List<User> users = userDao.queryForMatching(user);            
            if (users.isEmpty()) callback.onError("Username atau password salah!");
            else {
                activeUser = users.get(0);
                callback.onSuccess("Selamat datang kembali, "+activeUser.getFirstName());
            }
        } catch (SQLException ex) {
            System.err.println("Error while authenticating User with username ("+user.getUsername()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Users di database. Silahkan menghubungi admin.");
        }
    }
    
    public void createUser(User user, ICallback<User> callback) {
        try {
            File profile = new File(user.getImagePath());
            String path = FileUtils.copyFile(profile);
            user.setImagePath(path);
            
            userDao.create(user);
            callback.onSuccess(user);
        } catch (IOException ie) {
            System.err.println("Error while copying File : "+ie);
            callback.onError("Terjadi kesalahan ketika menyalin foto profil ke database. Silahkan menghubungi admin.");            
        } catch (SQLException ex) {
            System.err.println("Error while registering User with username ("+user.getUsername()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Users di database. Silahkan menghubungi admin.");
        }
    }
    
    public void updateUser(User user, ICallback<String> callback) {
        try {
            File profile = new File(user.getImagePath());
            String path = FileUtils.copyFile(profile);
            user.setImagePath(path);
            
            userDao.update(user);
            callback.onSuccess("User berhasil diperbarui.");
        } catch (IOException ie) {
            System.err.println("Error while copying File : "+ie);
            callback.onError("Terjadi kesalahan ketika menyalin berkas profile ke database. Silahkan menghubungi admin.");            
        } catch (SQLException ex) {
            System.err.println("Error while update User with uid ("+user.getUid()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Users di database. Silahkan menghubungi admin.");
        }
    }
    
    public void deleteUser(User user, ICallback<String> callback) {
        try {
            userDao.delete(user);
            callback.onSuccess("User berhasil dihapus.");
        } catch (SQLException ex) {
            System.err.println("Error while delete User with uid ("+user.getUid()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Users di database. Silahkan menghubungi admin.");
        }
    }
    
    public void getAvailableUser(ICallback<List<User>> callback) {
        try {
            List<User> list = userDao.queryForAll();
            callback.onSuccess(list);
        } catch (SQLException ex) {
            System.err.println("Error while load all User : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Users di database. Silahkan menghubungi admin.");
        }
    }
    
    public void upsertHeader(Header header, ICallback<String> callback) {
        try {
            File profile = new File(header.getImagePath());
            String path = FileUtils.copyFile(profile);
            header.setImagePath(path);
            
            header.setUid(1);
            headerDao.createOrUpdate(header);
            callback.onSuccess("Data header berhasil diperbarui.");
        } catch (IOException ie) {
            System.err.println("Error while copying File : "+ie);
            callback.onError("Terjadi kesalahan ketika menyalin berkas header ke database. Silahkan menghubungi admin.");            
        } catch (SQLException ex) {
            System.err.println("Error while update User with uid ("+header.getUid()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Headers di database. Silahkan menghubungi admin.");
        }
    }
    
    public void getHeader(ICallback<Header> callback) {
        try {
            Header header = headerDao.queryForId(1);
            callback.onSuccess(header);
        } catch (SQLException ex) {
            System.err.println("Error while load Header : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Headers di database. Silahkan menghubungi admin.");
        }
    }
    
    public void insertUnit(Unit unit, ICallback<String> callback) {
        try {
            unitDao.create(unit);
            callback.onSuccess("Berhasil menambahkan unit baru.");
        } catch (SQLException ex) {
            System.err.println("Error while insert Unit with uid ("+unit.getUid()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Unit di database. Silahkan menghubungi admin.");
        }
    }
    
    public void deleteUnit(Unit unit, ICallback<String> callback) {
        try {
            unitDao.delete(unit);
            callback.onSuccess("Berhasil menghapus unit.");
        } catch (SQLException ex) {
            System.err.println("Error while delete Unit with uid ("+unit.getUid()+") : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Unit di database. Silahkan menghubungi admin.");
        }
    }
    
    public void getUnit(ICallback<List<Unit>> callback) {
        try {
            List<Unit> list = unitDao.queryForAll();
            callback.onSuccess(list);
        } catch (SQLException ex) {
            System.err.println("Error while load all Unit : "+ex);
            callback.onError("Terjadi kesalahan ketika mengakses tabel Unit di database. Silahkan menghubungi admin.");
        }
    }
}
