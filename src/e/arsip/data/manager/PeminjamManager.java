/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.data.manager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import e.arsip.config.AppDatabase;
import e.arsip.data.model.Peminjam;
import e.arsip.util.DialogBuilder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Deri Armanda
 */
public class PeminjamManager {
    
    private static PeminjamManager SINGLETON;
    private final Dao<Peminjam, Integer> peminjamDao;
    
    private PeminjamManager() throws SQLException {
        peminjamDao = DaoManager.createDao(
                AppDatabase.getConnection(), 
                Peminjam.class
        );
    }
    
    public static void init() {
        try {
            SINGLETON = new PeminjamManager();
        } catch (SQLException ex) {
            SINGLETON = null;
            System.err.println("Error while creating Peminjam DAO: "+ex);
            DialogBuilder.showError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public static PeminjamManager getInstance() {
        if (SINGLETON == null) init();
        return SINGLETON;
    }
    
    public void insertPeminjam(Peminjam peminjam, ICallback<String> callback) {
        try {
            peminjamDao.create(peminjam);
            callback.onSuccess("Data Peminjam berhasil disimpan.");
        } catch (SQLException ex) {
            System.err.println("Error while insert Peminjam with uid ("+peminjam.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void updatePeminjam(Peminjam peminjam, ICallback<String> callback) {
        try {
            peminjamDao.update(peminjam);
            callback.onSuccess("Data Peminjam berhasil dirubah.");
        } catch (SQLException ex) {
            System.err.println("Error while update Peminjam with uid ("+peminjam.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void deletePeminjam(Peminjam peminjam, ICallback<String> callback) {
        try {
            peminjamDao.delete(peminjam);
            callback.onSuccess("Data Peminjam berhasil dihapus.");
        } catch (SQLException ex) {
            System.err.println("Error while delete Peminjam with uid ("+peminjam.getUid()+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void getPeminjamList(ICallback<List<Peminjam>> callback) {
        try {
            List<Peminjam> list = peminjamDao.queryForAll();
            callback.onSuccess(list);
        } catch (SQLException ex) {
            System.err.println("Error while load all Peminjam from Db: "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    public void getPeminjamListById(int suratUid, ICallback<List<Peminjam>> callback) {
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put(Peminjam.UID_SURAT_FIELD_NAME, suratUid);
            List<Peminjam> list = peminjamDao.queryForFieldValues(map);
            callback.onSuccess(list);
        } catch (SQLException ex) {
            System.err.println("Error while searching Peminjam with uid Surat ("+suratUid+") : "+ex);
            callback.onError(GENERAL_ERROR_MESSAGE);
        }
    }
    
    private static final String GENERAL_ERROR_MESSAGE = 
            "Terjadi kesalahan ketika mengakses tabel Peminjam di database. Silahkan menghubungi admin.";
    
}
