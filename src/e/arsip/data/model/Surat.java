/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import e.arsip.data.persister.SimpleStringPropertyPersister;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "surat")
public class Surat {
    
    private static final String NAMA_INSTANSI_FIELD_NAME = "nama_instansi";
    private static final String ALAMAT_FIELD_NAME = "alamat";
    private static final String KOTA_FIELD_NAME = "kota";
    private static final String NOMOR_SURAT_FIELD_NAME = "nomor_surat";
    private static final String PERIHAL_FIELD_NAME = "perihal";
    private static final String TANGGAL_SURAT_FIELD_NAME = "tanggal_surat";
    private static final String TANGGAL_SIMPAN_FIELD_NAME = "tanggal_simpan";
    private static final String JENIS_SURAT_FIELD_NAME = "jenis_surat";
    private static final String UNIT_FIELD_NAME = "unit";
    private static final String KERAHASIAAN_FIELD_NAME = "kerahasiaan";
    private static final String SISTEM_FIELD_NAME = "sistem";
    private static final String INDEX_FIELD_NAME = "index";
    private static final String KODE_FIELD_NAME = "kode";
    private static final String NOMOR_URUT_FIELD_NAME = "nomor_urut";
    private static final String LAMPIRAN_FIELD_NAME = "lampiran";
    private static final String RINGKASAN_FIELD_NAME = "ringkasan";
    private static final String CATATAN_FIELD_NAME = "catatan";
    private static final String PETUGAS_FIELD_NAME = "petugas";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(            
            columnName = NAMA_INSTANSI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaInstansi;
    
    @DatabaseField(            
            columnName = ALAMAT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty alamat;
    
    @DatabaseField(            
            columnName = KOTA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kota;
    
    @DatabaseField(            
            columnName = NOMOR_SURAT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorSurat;
    
    @DatabaseField(            
            columnName = PERIHAL_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty perihal;
    
    @DatabaseField(            
            columnName = TANGGAL_SURAT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalSurat;
    
    @DatabaseField(            
            columnName = TANGGAL_SIMPAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalSimpan;
    
    @DatabaseField(            
            columnName = JENIS_SURAT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jenisSurat;
    
    @DatabaseField(            
            columnName = UNIT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty unit;
    
    @DatabaseField(            
            columnName = KERAHASIAAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kerahasiaan;
    
    @DatabaseField(            
            columnName = SISTEM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty sistem;
    
    @DatabaseField(            
            columnName = INDEX_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty index;
    
    @DatabaseField(            
            columnName = KODE_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty kode;
    
    @DatabaseField(            
            columnName = NOMOR_URUT_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nomorUrut;
    
    @DatabaseField(            
            columnName = LAMPIRAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty lampiran;
    
    @DatabaseField(            
            columnName = RINGKASAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty ringkasan;
    
    @DatabaseField(            
            columnName = CATATAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty catatan;
    
    @DatabaseField(            
            columnName = PETUGAS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty petugas;
    
    private boolean isDipinjam;
    private Peminjam peminjam;
    private List<Attachment> attachments;
    
    public Surat() {
        isDipinjam = false;
    }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getNamaInstansi() {
        return namaInstansi.get();
    }
    public void setNamaInstansi(String namaInstansi) {
        if (this.namaInstansi == null) this.namaInstansi = new SimpleStringProperty(namaInstansi);
        else this.namaInstansi.set(namaInstansi);
    }
    public StringProperty namaInstansiProperty() {
        return namaInstansi;
    }    
    
    public String getAlamat() {
        return alamat.get();
    }
    public void setAlamat(String alamat) {
        if (this.alamat == null) this.alamat = new SimpleStringProperty(alamat);
        else this.alamat.set(alamat);
    }
    public StringProperty alamatProperty() {
        return alamat;
    }   
    
    public String getKota() {
        return kota.get();
    }
    public void setKota(String kota) {
        if (this.kota == null) this.kota = new SimpleStringProperty(kota);
        else this.kota.set(kota);
    }
    public StringProperty kotaProperty() {
        return kota;
    }   
    
    public String getNomorSurat() {
        return nomorSurat.get();
    }
    public void setNomorSurat(String nomorSurat) {
        if (this.nomorSurat == null) this.nomorSurat = new SimpleStringProperty(nomorSurat);
        else this.nomorSurat.set(nomorSurat);
    }
    public StringProperty nomorSuratProperty() {
        return nomorSurat;
    }   
    
    public String getPerihal() {
        return perihal.get();
    }
    public void setPerihal(String perihal) {
        if (this.perihal == null) this.perihal = new SimpleStringProperty(perihal);
        else this.perihal.set(perihal);
    }
    public StringProperty perihalProperty() {
        return perihal;
    }   
    
    public String getTanggalSurat() {
        return tanggalSurat.get();
    }
    public void setTanggalSurat(String tanggalSurat) {
        if (this.tanggalSurat == null) this.tanggalSurat = new SimpleStringProperty(tanggalSurat);
        else this.tanggalSurat.set(tanggalSurat);
    }
    public StringProperty tanggalSuratProperty() {
        return tanggalSurat;
    }   
    
    public String getTanggalSimpan() {
        return tanggalSimpan.get();
    }
    public void setTanggalSimpan(String tanggalSimpan) {
        if (this.tanggalSimpan == null) this.tanggalSimpan = new SimpleStringProperty(tanggalSimpan);
        else this.tanggalSimpan.set(tanggalSimpan);
    }
    public StringProperty tanggalSimpanProperty() {
        return tanggalSimpan;
    }   
    
    public String getJenisSurat() {
        return jenisSurat.get();
    }
    public void setJenisSurat(String jenisSurat) {
        if (this.jenisSurat == null) this.jenisSurat = new SimpleStringProperty(jenisSurat);
        else this.jenisSurat.set(jenisSurat);
    }
    public StringProperty jenisSuratProperty() {
        return jenisSurat;
    }   
    
    public String getUnit() {
        return unit.get();
    }
    public void setUnit(String unit) {
        if (this.unit == null) this.unit = new SimpleStringProperty(unit);
        else this.unit.set(unit);
    }
    public StringProperty unitProperty() {
        return unit;
    }   
    
    public String getKerahasiaan() {
        return kerahasiaan.get();
    }
    public void setKerahasiaan(String kerahasiaan) {
        if (this.kerahasiaan == null) this.kerahasiaan = new SimpleStringProperty(kerahasiaan);
        else this.kerahasiaan.set(kerahasiaan);
    }
    public StringProperty kerahasiaanProperty() {
        return kerahasiaan;
    }   
    
    public String getSistem() {
        return sistem.get();
    }
    public void setSistem(String sistem) {
        if (this.sistem == null) this.sistem = new SimpleStringProperty(sistem);
        else this.sistem.set(sistem);
    }
    public StringProperty sistemProperty() {
        return sistem;
    }   
    
    public String getIndex() {
        return index.get();
    }
    public void setIndex(String index) {
        if (this.index == null) this.index = new SimpleStringProperty(index);
        else this.index.set(index);
    }
    public StringProperty indexProperty() {
        return index;
    }   
    
    public String getKode() {
        return kode.get();
    }
    public void setKode(String kode) {
        if (this.kode == null) this.kode = new SimpleStringProperty(kode);
        else this.kode.set(kode);
    }
    public StringProperty kodeProperty() {
        return kode;
    }   
    
    public String getNomorUrut() {
        return nomorUrut.get();
    }
    public void setNomorUrut(String nomorUrut) {
        if (this.nomorUrut == null) this.nomorUrut = new SimpleStringProperty(nomorUrut);
        else this.nomorUrut.set(nomorUrut);
    }
    public StringProperty nomorUrutProperty() {
        return nomorUrut;
    }   
    
    public String getLampiran() {
        return lampiran.get();
    }
    public void setLampiran(String lampiran) {
        if (this.lampiran == null) this.lampiran = new SimpleStringProperty(lampiran);
        else this.lampiran.set(lampiran);
    }
    public StringProperty lampiranProperty() {
        return lampiran;
    }   
    
    public String getRingkasan() {
        return ringkasan.get();
    }
    public void setRingkasan(String ringkasan) {
        if (this.ringkasan == null) this.ringkasan = new SimpleStringProperty(ringkasan);
        else this.ringkasan.set(ringkasan);
    }
    public StringProperty ringkasanProperty() {
        return ringkasan;
    }   
    
    public String getCatatan() {
        return catatan.get();
    }
    public void setCatatan(String catatan) {
        if (this.catatan == null) this.catatan = new SimpleStringProperty(catatan);
        else this.catatan.set(catatan);
    }
    public StringProperty catatanProperty() {
        return catatan;
    }   
    
    public String getPetugas() {
        return petugas.get();
    }
    public void setPetugas(String petugas) {
        if (this.petugas == null) this.petugas = new SimpleStringProperty(petugas);
        else this.petugas.set(petugas);
    }
    public StringProperty petugasProperty() {
        return petugas;
    }   

    public boolean isDipinjam() {
        return isDipinjam;
    }

    public void setIsDipinjam(boolean isDipinjam) {
        this.isDipinjam = isDipinjam;
    }

    public Peminjam getPeminjam() {
        return peminjam;
    }

    public void setPeminjam(Peminjam peminjam) {
        this.peminjam = peminjam;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }
    
    public StringProperty laciProperty() {
        String sistemSimpan = sistem.get(), laci = "-";
        if (sistemSimpan.equals("Abjad")) {
            byte b = (byte) kode.get().toLowerCase().charAt(0);
            if (b >= 97 && b <= 101) laci = "A-E";
            else if (b >= 102 && b <= 106) laci = "F-J";
            else if (b >= 107 && b <= 111) laci = "K-O";
            else if (b >= 112 && b <= 116) laci = "P-T";
            else laci = "U-Z";
        } else if (sistemSimpan.equals("Tanggal Surat")) {
            laci = kode.get().split("/")[2];
        }
        return new SimpleStringProperty(laci);
    }   
    
    public StringProperty guideProperty() {
        String sistemSimpan = sistem.get(), guide = "-";
        if (sistemSimpan.equals("Abjad")) {
            guide = kode.get().toUpperCase().charAt(0)+"";
        } else if (sistemSimpan.equals("Tanggal Surat")) {
            guide = kode.get().split("/")[1];
        }
        return new SimpleStringProperty(guide);
    }   
    
    public StringProperty mapProperty() {
        String sistemSimpan = sistem.get(), map = "-";
        if (sistemSimpan.equals("Abjad")) {
            map = kode.get().toUpperCase();
        } else if (sistemSimpan.equals("Tanggal Surat")) {
            map = kode.get().split("/")[0];
        }
        return new SimpleStringProperty(map);
    }   

    @Override
    public String toString() {
        return getNamaInstansi() + "    " + getTanggalSurat() + "    " + getPerihal() + "    " + getKode();
    }
    
}
