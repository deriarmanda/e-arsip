/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import e.arsip.data.persister.SimpleStringPropertyPersister;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "units")
public class Unit {
        
    public static final String NAMA_FIELD_NAME = "nama";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(            
            columnName = NAMA_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty nama;
    
    public Unit() { }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
    
    public String getNama() {
        return nama.get();
    }
    public void setNama(String nama) {
        if (this.nama == null) this.nama = new SimpleStringProperty(nama);
        else this.nama.set(nama);
    }
    public StringProperty namaProperty() {
        return nama;
    }   
}
