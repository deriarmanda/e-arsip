/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import e.arsip.data.persister.SimpleStringPropertyPersister;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Deri Armanda
 */
@DatabaseTable(tableName = "peminjam")
public class Peminjam {
    
    public static final String UID_SURAT_FIELD_NAME = "uid_surat";
    public static final String STATUS_FIELD_NAME = "status";
    public static final String TANGGAL_PINJAM_FIELD_NAME = "tanggal_pinjam";
    public static final String TANGGAL_KEMBALI_FIELD_NAME = "tanggal_kembali";
    public static final String NAMA_PEMINJAM_FIELD_NAME = "nama_peminjam";
    public static final String JABATAN_FIELD_NAME = "jabatan";
    public static final String PETUGAS_FIELD_NAME = "petugas";
    
    @DatabaseField(generatedId = true) 
    private int uid;
    
    @DatabaseField(
            canBeNull = false, 
            foreign = true,
            columnName = UID_SURAT_FIELD_NAME)
    private Surat surat;
    
    @DatabaseField(
            columnName = STATUS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty status;
    
    @DatabaseField(
            columnName = TANGGAL_PINJAM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalPinjam;
    
    @DatabaseField(
            columnName = TANGGAL_KEMBALI_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty tanggalKembali;
    
    @DatabaseField(
            columnName = NAMA_PEMINJAM_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty namaPeminjam;
    
    @DatabaseField(
            columnName = JABATAN_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty jabatan;
    
    @DatabaseField(
            columnName = PETUGAS_FIELD_NAME,
            persisterClass = SimpleStringPropertyPersister.class
    ) private StringProperty petugas;
    
    public Peminjam() { }
    
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }

    public Surat getSurat() {
        return surat;
    }

    public void setSurat(Surat surat) {
        this.surat = surat;
    }
    
    public String getStatus() {
        return status.get();
    }
    public void setStatus(String status) {
        if (this.status == null) this.status = new SimpleStringProperty(status);
        else this.status.set(status);
    }
    public StringProperty statusProperty() {
        return status;
    }
    
    public String getTanggalPinjam() {
        return tanggalPinjam.get();
    }
    public void setTanggalPinjam(String tanggalPinjam) {
        if (this.tanggalPinjam == null) this.tanggalPinjam = new SimpleStringProperty(tanggalPinjam);
        else this.tanggalPinjam.set(tanggalPinjam);
    }
    public StringProperty tanggalPinjamProperty() {
        return tanggalPinjam;
    }
    
    public String getTanggalKembali() {
        return tanggalKembali.get();
    }
    public void setTanggalKembali(String tanggalKembali) {
        if (this.tanggalKembali == null) this.tanggalKembali = new SimpleStringProperty(tanggalKembali);
        else this.tanggalKembali.set(tanggalKembali);
    }
    public StringProperty tanggalKembaliProperty() {
        return tanggalKembali;
    }
    
    public String getNamaPeminjam() {
        return namaPeminjam.get();
    }
    public void setNamaPeminjam(String namaPeminjam) {
        if (this.namaPeminjam == null) this.namaPeminjam = new SimpleStringProperty(namaPeminjam);
        else this.namaPeminjam.set(namaPeminjam);
    }
    public StringProperty namaPeminjamProperty() {
        return namaPeminjam;
    }
    
    public String getJabatan() {
        return jabatan.get();
    }
    public void setJabatan(String jabatan) {
        if (this.jabatan == null) this.jabatan = new SimpleStringProperty(jabatan);
        else this.jabatan.set(jabatan);
    }
    public StringProperty jabatanProperty() {
        return jabatan;
    }
    
    public String getPetugas() {
        return petugas.get();
    }
    public void setPetugas(String petugas) {
        if (this.petugas == null) this.petugas = new SimpleStringProperty(petugas);
        else this.petugas.set(petugas);
    }
    public StringProperty petugasProperty() {
        return petugas;
    }
}
