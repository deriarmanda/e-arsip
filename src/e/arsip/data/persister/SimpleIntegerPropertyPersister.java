/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.data.persister;

import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseDataType;
import com.j256.ormlite.support.DatabaseResults;
import java.sql.SQLException;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Deri Armanda
 */
public class SimpleIntegerPropertyPersister extends BaseDataType {

    private static final SimpleIntegerPropertyPersister SINGLETON = 
            new SimpleIntegerPropertyPersister();

    private SimpleIntegerPropertyPersister() {
        super(SqlType.INTEGER, new Class<?>[]{Integer.class});
    }

    /**
     * Parse a default String into a SimpleIntegerProperty.
     * @param fieldType Field Type
     * @param defaultStr Default string to parse
     * @return New SimpleIntegerProperty
     */
    @Override
    public Object parseDefaultString(FieldType fieldType, String defaultStr) {
        return new SimpleIntegerProperty(Integer.parseInt(defaultStr));
    }

    /**
     * Getter for singleton of class.
     * @return
     */
    public static SimpleIntegerPropertyPersister getSingleton() {
        return SINGLETON;
    }

    /**
     * Converts database results into Sql argument
     * @param fieldType Field Type
     * @param results Results to convert
     * @param columnPos Column Position
     * @return SQL argument from input
     * @throws SQLException
     */
    @Override
    public Object resultToSqlArg(FieldType fieldType, DatabaseResults results, int columnPos) throws SQLException {
        return results.getInt(columnPos);
    }


    /**
     * Converts SQL argument into a SimpleIntegerProperty.
     * @param fieldType Field Type
     * @param sqlArg SQL argument to convert
     * @param columnPos Column Position
     * @return SimpleIntegerProperty form input
     */
    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        return new SimpleIntegerProperty((Integer) sqlArg);
    }

    /**
    * Converts a SimpleIntegerProperty into a int to be store in the database.
    * @param fieldType Field Type
    * @param javaObject SimpleIntegerProperty to convert to integer
    * @return Integer from input
     * @throws SQLException
    */
    @Override
    public Object javaToSqlArg(FieldType fieldType, Object javaObject) throws SQLException {
        SimpleIntegerProperty integerProperty = (SimpleIntegerProperty) javaObject;
        return integerProperty.getValue();
    }
    
}
