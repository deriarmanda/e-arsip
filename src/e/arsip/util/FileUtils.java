/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.util;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Deri Armanda
 */
public class FileUtils {
    
    private static final FileChooser FILE_CHOOSER = new FileChooser();
    
    public static String copyFile(File copied) throws IOException {
        createParentDir();
        File target = new File(System.getProperty("user.dir")
                +File.separator+"files"+File.separator+copied.getName());
        target.createNewFile();
        FileOutputStream out;
        try (InputStream in = new FileInputStream(copied)) {
            out = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            int read;
            while((read = in.read(buffer)) != -1){
                out.write(buffer, 0, read);
            }
        }
        out.close();
        return target.getPath();
    }
    
    public static void openFile(String path) {
        openFile(new File(path));
    }
    
    public static void openFile(File file) {
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException ex) {
            System.err.println("Error while opening File ("+file.getName()+"): "+ex);  
            DialogBuilder.showError("Gagal membuka berkas "+file.getName());          
        }
    }
    
    public static File openFileDialog(Stage stage) {
        return FILE_CHOOSER.showOpenDialog(stage);
    }
    
    public static File saveFile(Stage stage) {
        return FILE_CHOOSER.showSaveDialog(stage);
    }
    
    private static void createParentDir() throws IOException {        
        File target = new File(System.getProperty("user.dir")+File.separator+"files");
        if (!target.exists()) {
            System.out.println("create dir");
            target.mkdirs();
        }
    }
}
