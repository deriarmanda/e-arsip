/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.config;

import javafx.scene.image.Image;

/**
 *
 * @author Deri Armanda
 */
public class AppConfig {
    
    public static final String APP_NAME = "E - Arsip";
    public static final Image APP_LOGO = new Image("/e/arsip/icon/logo_app.png");
    
}
