/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.User;
import e.arsip.util.DialogBuilder;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Deri Armanda
 */
public class LoginController implements Initializable {

    @FXML
    private TextField tf_username;
    @FXML
    private PasswordField pf_password;
    
    private UserManager userManager;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
    }    

    @FXML
    private void btn_login(ActionEvent event) {
        //onLoginSuccess((Node) event.getSource());
        User user = new User();
        user.setUsername(tf_username.getText());
        user.setPassword(pf_password.getText());
        
        userManager.authenticate(user, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
                onLoginSuccess((Node) event.getSource());
            }

            @Override
            public void onError(String message) {
                onLoginError(message);
            }
        });
    }
    
    private void onLoginSuccess(Node node) {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Home.fxml"));
            Stage stage = (Stage) node.getScene().getWindow();
            stage.setScene(new Scene(root));
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void onLoginError(String message) {
        DialogBuilder.showError(message);
    }
    
}
