/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.PeminjamManager;
import e.arsip.data.manager.SuratManager;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Peminjam;
import e.arsip.data.model.Surat;
import e.arsip.data.model.User;
import e.arsip.util.DateTimeUtils;
import e.arsip.util.DialogBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class PeminjamanController implements Initializable {

    @FXML
    private TextField namaPeminjam;
    @FXML
    private DatePicker tanggalPinjam;
    @FXML
    private TextField jabatan;
    @FXML
    private TextField namaInstansi;
    @FXML
    private TextField tanggalSurat;
    @FXML
    private TextField perihal;
    @FXML
    private TextField kode;
    @FXML
    private DatePicker tanggalKembali;
    @FXML
    private ComboBox<String> petugas;
    private ImageView fotoProfile;
    private Text userName;
    @FXML
    private ComboBox<Surat> pilihData;

    /**
     * Initializes the controller class.
     */
    
    void btn_home(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Home.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pengaturan(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pengaturan.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    private void btn_tentang(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Tentang.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_logout(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_input(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Input.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_manajemen(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Manajemen.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_rekap(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Rekap.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_bukuagenda(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Bukuagenda.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_peminjaman(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Peminjaman.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pencarian(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pencarian.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_jra(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Jra.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_statistik(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Statistik.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    @FXML
    private void btnPinjam(MouseEvent event) {
        Peminjam peminjam = new Peminjam();
        peminjam.setNamaPeminjam(namaPeminjam.getText());
        peminjam.setJabatan(jabatan.getText());
        peminjam.setPetugas(petugas.getValue());
        peminjam.setStatus("dipinjam");
        peminjam.setSurat(selectedSurat);
        peminjam.setTanggalKembali(DateTimeUtils.format(tanggalKembali.getValue()));
        peminjam.setTanggalPinjam(DateTimeUtils.format(tanggalPinjam.getValue()));
        
        peminjamManager.insertPeminjam(peminjam, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                namaPeminjam.clear();
                jabatan.clear();
                tanggalKembali.setValue(null);
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }
    
    private SuratManager suratManager;
    private PeminjamManager peminjamManager;
    private UserManager userManager;
    private ObservableList<Surat> suratList;
    private Surat selectedSurat;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratManager = SuratManager.getInstance();
        userManager = UserManager.getInstance();
        peminjamManager = PeminjamManager.getInstance();
        suratList = FXCollections.observableArrayList();
        selectedSurat = new Surat();
        pilihData.setItems(suratList);
        
        tanggalKembali.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalPinjam.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalPinjam.setValue(LocalDate.now());
        
        userManager.getAvailableUser(new ICallback<List<User>>() {
            @Override
            public void onSuccess(List<User> data) {
                for (User user : data) petugas.getItems().add(user.getFullName());
                petugas.getSelectionModel().selectFirst();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
        pilihData.valueProperty().addListener((ObservableValue<? extends Surat> observable, Surat oldValue, Surat newValue) -> {
            selectedSurat = newValue;
            fetchSurat();
        });
    }    
    
    private void fetchSurat() {
        namaInstansi.setText(selectedSurat.getNamaInstansi());
        tanggalSurat.setText(selectedSurat.getTanggalSurat());
        perihal.setText(selectedSurat.getPerihal());
        kode.setText(selectedSurat.getKode());
    }
}
