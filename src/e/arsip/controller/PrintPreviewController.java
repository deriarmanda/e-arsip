/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Header;
import e.arsip.data.model.Surat;
import e.arsip.util.DialogBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class PrintPreviewController implements Initializable {

    @FXML
    private ImageView logo;
    @FXML
    private Text header1;
    @FXML
    private Text header2;
    @FXML
    private Text header3;
    @FXML
    private Text header4;
    @FXML
    private Text kontak;
    @FXML
    private TableView<Surat> bukuAgenda;
    @FXML
    private TableColumn<Surat, String> mk;
    @FXML
    private TableColumn<Surat, String> kepada;
    @FXML
    private TableColumn<Surat, String> tanggalSurat;
    @FXML
    private TableColumn<Surat, String> nomorSurat;
    @FXML
    private TableColumn<Surat, String> hal;
    @FXML
    private TableColumn<Surat, String> kode;
    @FXML
    private TableColumn<Surat, String> laci;
    @FXML
    private TableColumn<Surat, String> guide;
    @FXML
    private TableColumn<Surat, String> map;

    private ObservableList<Surat> tableData;
    private VBox printLayout;

    public void setTableData(ObservableList<Surat> tableData) {
        this.tableData.clear();
        this.tableData.addAll(tableData);
    }
    
    @FXML
    private void print(MouseEvent event) {
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(printLayout.getScene().getWindow())){
            boolean success = job.printPage(printLayout);
            if (success) {
                job.endJob();
            }
        }
    }
       
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tableData = FXCollections.observableArrayList();
        
        mk.setCellValueFactory(value -> value.getValue().jenisSuratProperty());
        nomorSurat.setCellValueFactory(value -> value.getValue().nomorSuratProperty());
        tanggalSurat.setCellValueFactory(value -> value.getValue().tanggalSuratProperty());
        kepada.setCellValueFactory(value -> value.getValue().namaInstansiProperty());
        hal.setCellValueFactory(value -> value.getValue().perihalProperty());
        kode.setCellValueFactory(value -> value.getValue().kodeProperty());
        laci.setCellValueFactory(value -> value.getValue().laciProperty());
        guide.setCellValueFactory(value -> value.getValue().guideProperty());
        map.setCellValueFactory(value -> value.getValue().mapProperty());
        bukuAgenda.setItems(tableData);
        
        UserManager userManager = UserManager.getInstance(); 
        userManager.getHeader(new ICallback<Header>() {
            @Override
            public void onSuccess(Header data) {
                if (data == null) {
                    DialogBuilder.showError("Silahkan buat header surat terlebih dahulu di menu Pengaturan!");
                } else {
                    File foto = new File(data.getImagePath());
                    logo.setImage(
                            new Image(foto.toURI().toString(), 
                            logo.getFitWidth(),
                            logo.getFitHeight(),
                            false, false
                    ));
                    header1.setText(data.getHeader1());
                    header2.setText(data.getHeader2());
                    header3.setText(data.getHeader3());
                    header4.setText(data.getHeader4());
                    kontak.setText(data.getKontak());
                }
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
    }
    
}
