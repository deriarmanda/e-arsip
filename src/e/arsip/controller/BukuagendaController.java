/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.SuratManager;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Surat;
import e.arsip.data.model.User;
import e.arsip.util.DateTimeUtils;
import e.arsip.util.DialogBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class BukuagendaController implements Initializable {

    @FXML
    private TableView<Surat> bukuAgenda;
    @FXML
    private TableColumn<Surat, String> mk;
    @FXML
    private TableColumn<Surat, String> nomorSurat;
    @FXML
    private TableColumn<Surat, String> tanggalSurat;
    @FXML
    private TableColumn<Surat, String> kepada;
    @FXML
    private TableColumn<Surat, String> hal;
    @FXML
    private TableColumn<Surat, String> kode;
    private ImageView fotoProfile;
    private Text userName;
    @FXML
    private ComboBox<String> filterSistemSimpan;
    @FXML
    private ComboBox<String> filterTahun;
    @FXML
    private TableColumn<Surat, String> laci;
    @FXML
    private TableColumn<Surat, String> guide;
    @FXML
    private TableColumn<Surat, String> map;
    
    private UserManager userManager;
    private SuratManager suratManager;
    private ObservableList<Surat> suratList, tableList;
    private HomeController homeController;

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private void print(MouseEvent event) throws IOException {
        FXMLLoader loader =  new FXMLLoader(
                getClass().getResource("/e/arsip/view/printPreview.fxml")
        );
        
        Parent root = loader.load();
        PrintPreviewController controller = (PrintPreviewController) loader.getController();
        controller.setTableData(tableList);
        
        if (homeController != null) homeController.showPage(root);
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
        
        suratList = FXCollections.observableArrayList();
        tableList = FXCollections.observableArrayList();
        
        mk.setCellValueFactory(value -> value.getValue().jenisSuratProperty());
        nomorSurat.setCellValueFactory(value -> value.getValue().nomorSuratProperty());
        tanggalSurat.setCellValueFactory(value -> value.getValue().tanggalSuratProperty());
        kepada.setCellValueFactory(value -> value.getValue().namaInstansiProperty());
        hal.setCellValueFactory(value -> value.getValue().perihalProperty());
        kode.setCellValueFactory(value -> value.getValue().kodeProperty());
        laci.setCellValueFactory(value -> value.getValue().laciProperty());
        guide.setCellValueFactory(value -> value.getValue().guideProperty());
        map.setCellValueFactory(value -> value.getValue().mapProperty());
        bukuAgenda.setItems(tableList);
        
        suratManager = SuratManager.getInstance();
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
                collectYear();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        filterSistemSimpan.getItems().add("Abjad");
        filterSistemSimpan.getItems().add("Tanggal Surat");
//        filterSistemSimpan.getItems().add("Desimal");
//        filterSistemSimpan.getItems().add("Terminal Digit");
//        filterSistemSimpan.getItems().add("Wilayah");
        filterSistemSimpan.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String tahun = filterTahun.getValue();
            tableList.clear();
            for (Surat surat : suratList) {
                String y = DateTimeUtils.formatYearOnly(
                        DateTimeUtils.parseDateOnly(surat.getTanggalSimpan())
                );
                if (surat.getSistem().equalsIgnoreCase(newValue) &&
                        y.equals(tahun)) {
                    tableList.add(surat);
                }
            }
        });
        filterTahun.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String sistem = filterSistemSimpan.getValue();
            tableList.clear();
            for (Surat surat : suratList) {
                String y = DateTimeUtils.formatYearOnly(
                        DateTimeUtils.parseDateOnly(surat.getTanggalSimpan())
                );
                if (y.equals(newValue) && surat.getSistem().equalsIgnoreCase(sistem)) {
                    tableList.add(surat);
                }
            }
        });
    }
    
    public void setHomeController(HomeController controller) {
        this.homeController = controller;
    }
    
    private void collectYear() {
        for (Surat surat : suratList) {
            String year = DateTimeUtils.formatYearOnly(
                    DateTimeUtils.parseDateOnly(surat.getTanggalSimpan())
            );
            if (!filterTahun.getItems().contains(year)) filterTahun.getItems().add(year);
        }
    }
    
}
