/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.PeminjamManager;
import e.arsip.data.manager.SuratManager;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Attachment;
import e.arsip.data.model.Peminjam;
import e.arsip.data.model.Surat;
import e.arsip.data.model.Unit;
import e.arsip.data.model.User;
import e.arsip.util.DateTimeUtils;
import e.arsip.util.DialogBuilder;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class ManajemenController implements Initializable {

    @FXML
    private ComboBox<Surat> pilihData;
    @FXML
    private TextField namaInstansi;
    @FXML
    private TextField alamat;
    @FXML
    private TextField kota;
    @FXML
    private TextField nomorSurat;
    @FXML
    private TextField perihal;
    @FXML
    private DatePicker tanggalSurat;
    @FXML
    private DatePicker tanggalSimpan;
    @FXML
    private ComboBox<String> unit;
    @FXML
    private ComboBox<String> brsr;
    @FXML
    private ComboBox<String> sistem;
    @FXML
    private TextField index;
    @FXML
    private TextField kode;
    @FXML
    private TextField nomorUrut;
    @FXML
    private TextField lampiran;
    @FXML
    private TextArea catatan;
    @FXML
    private Button btnTambah;
    @FXML
    private Button btnHapus;    
    @FXML
    private TextArea isiRingkasan;
    @FXML
    private ListView<String> listAttachment1;
    @FXML
    private TextField namaPeminjam;
    @FXML
    private TextField tanggalPinjam;
    @FXML
    private TextField jabatan;
    @FXML
    private TextField tanggalKembali;
    @FXML
    private TextField petugas;
    @FXML
    private TableView<Peminjam> riwayatPinjam;
    @FXML
    private HBox pinjamArsip;
    @FXML
    private HBox pengembalianArsip;
    @FXML
    private HBox simpanArsip;
    @FXML
    private HBox hapusArsip;
    @FXML
    private Text statusSurat;
    @FXML
    private TextField statusArsip;
    @FXML
    private ComboBox<String> jenisSurat;
    @FXML
    private TableColumn<Peminjam, String> cNama;
    @FXML
    private TableColumn<Peminjam, String> cTanggalPinjam;
    @FXML
    private TableColumn<Peminjam, String> cTanggalKembali;
    @FXML
    private TableColumn<Peminjam, String> cPetugas;
    @FXML
    private DatePicker tanggalPengambalian;
    @FXML
    private TextField petugasPengembalian;
    @FXML
    private HBox simpanPengembalian;
    @FXML
    private HBox hapusPengembalian;
    @FXML
    private VBox formPengembalian;
    private ImageView fotoProfile;
    private Text userName;
    
    /**
     * Initializes the controller class.
     */
    
    private SuratManager suratManager;
    private PeminjamManager peminjamManager;
    private UserManager userManager;
    private ObservableList<Surat> suratList;
    private ObservableList<Peminjam> peminjamList;
    private Surat selectedSurat;
    private Peminjam selectedPeminjam;
    private ArrayList<Attachment> attachments;
    private ObservableList<String> listAtt;
    private HomeController homeController;
    @FXML
    private HBox printKk;
    @FXML
    private TextField kode1;
    @FXML
    private TextField kode11;
    
    @FXML
    private void ButtonTambah(ActionEvent event) {
        
    }

    @FXML
    private void pinjamArsip(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Peminjaman.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    @FXML
    private void pengembalianArsip(MouseEvent event) {
        if (selectedSurat.isDipinjam()) formPengembalian.setVisible(true);
    }
    
    @FXML
    private void simpanArsip(MouseEvent event) {
        selectedSurat.setNamaInstansi(namaInstansi.getText());
        selectedSurat.setAlamat(alamat.getText());
        selectedSurat.setKota(kota.getText());
        selectedSurat.setNomorSurat(nomorSurat.getText());
        selectedSurat.setPerihal(perihal.getText());
        selectedSurat.setTanggalSurat(DateTimeUtils.format(tanggalSurat.getValue()));
        selectedSurat.setTanggalSimpan(DateTimeUtils.format(tanggalSimpan.getValue()));
        selectedSurat.setJenisSurat(jenisSurat.getValue());
        selectedSurat.setUnit(unit.getValue());
        selectedSurat.setKerahasiaan(brsr.getValue());
        selectedSurat.setSistem(sistem.getValue());
        selectedSurat.setIndex(index.getText());
        selectedSurat.setKode(kode.getText()+(kode1.isDisable()? "" : "/"+kode1.getText()+"/"+kode11.getText()));
        selectedSurat.setNomorUrut(nomorUrut.getText());
        selectedSurat.setLampiran(lampiran.getText());
        selectedSurat.setRingkasan(isiRingkasan.getText());
        selectedSurat.setCatatan(catatan.getText());       
        
        suratManager.updateSurat(selectedSurat, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }

    @FXML
    private void hapusArsip(MouseEvent event) {
        suratManager.deleteSurat(selectedSurat, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                suratList.remove(selectedSurat);
                pilihData.getSelectionModel().selectFirst();
                DialogBuilder.showInfo(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }
    

    @FXML
    private void simpanPengembalian(MouseEvent event) {
        selectedPeminjam.setStatus("kembali");
        selectedPeminjam.setPetugas(petugasPengembalian.getText());
        selectedPeminjam.setTanggalKembali(DateTimeUtils.format(tanggalPengambalian.getValue()));
        
        peminjamManager.updatePeminjam(selectedPeminjam, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                statusSurat.setVisible(false);
                namaPeminjam.clear();
                statusArsip.setText("Tersedia");
                jabatan.clear();
                petugas.clear();
                tanggalPinjam.clear();
                tanggalKembali.clear();
                suratManager.setStatusSurat(selectedSurat.getUid(), false);
                DialogBuilder.showInfo("Data pengembalian berhasil disimpan.");
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
    }

    @FXML
    private void hapusPengembalian(MouseEvent event) {
        formPengembalian.setVisible(false);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratManager = SuratManager.getInstance();    
        userManager = UserManager.getInstance();
        peminjamManager = PeminjamManager.getInstance();
        
        selectedSurat = new Surat();
        attachments = new ArrayList();
        suratList = FXCollections.observableArrayList();
        peminjamList = FXCollections.observableArrayList();
        pilihData.setItems(suratList);
        listAtt = FXCollections.observableArrayList();
        listAttachment1.setItems(listAtt);
        listAttachment1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    int index = listAttachment1.getSelectionModel().getSelectedIndex();
                    if (index >= 0 && index < attachments.size()) {
                        Attachment selected = attachments.get(index);
                        try {
                            Desktop.getDesktop().open(new File(selected.getPathFile()));
                        } catch (IOException ex) {
                            DialogBuilder.showError("Gagal membuka file yg dipilih.\nEx: "+ex.getMessage());
                        }
                    }
                }
            }
        });
        
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
        });
        
        tanggalSurat.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setValue(LocalDate.now());
        tanggalPengambalian.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalPengambalian.setValue(LocalDate.now());
        
        jenisSurat.getItems().add("Masuk");
        jenisSurat.getItems().add("Keluar");
        jenisSurat.getSelectionModel().selectFirst();       
        
        userManager.getUnit(new ICallback<List<Unit>>() {
            @Override
            public void onSuccess(List<Unit> data) {
                unit.getItems().clear();
                for (Unit u : data) {
                    unit.getItems().add(u.getNama());
                }
                unit.getSelectionModel().selectFirst();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        brsr.getItems().add("Biasa");
        brsr.getItems().add("Rahasia");
        brsr.getItems().add("Sangat Rahasia");
        brsr.getSelectionModel().selectFirst();
        
        sistem.getItems().add("Abjad");
        sistem.getItems().add("Tanggal Surat");
//        sistem.getItems().add("Desimal");
//        sistem.getItems().add("Terminal Digit");
//        sistem.getItems().add("Wilayah");
        sistem.getSelectionModel().selectFirst();
        
//        Callback<ListView<Surat>, ListCell<Surat>> cellFactory = (ListView<Surat> param) -> new ListCell<Surat>() {
//            @Override
//            protected void updateItem(Surat item, boolean empty) {
//                super.updateItem(item, empty);
//                if (item == null || empty) {
//                    setGraphic(null);
//                } else {
//                    setText(item.getNamaInstansi() + "    " + item.getNomorSurat() + "    " + item.getPerihal());
//                }
//            }            
//        };
//        pilihData.setButtonCell(cellFactory.call(null));
//        pilihData.setCellFactory(cellFactory);
        pilihData.valueProperty().addListener((ObservableValue<? extends Surat> observable, Surat oldValue, Surat newValue) -> {
            selectedSurat = newValue;
            fetchSurat();
        });
        
        petugasPengembalian.setText(userManager.getActiveUser().getFullName());
        
        cNama.setCellValueFactory(value -> value.getValue().namaPeminjamProperty());
        cTanggalPinjam.setCellValueFactory(value -> value.getValue().tanggalPinjamProperty());
        cTanggalKembali.setCellValueFactory(value -> value.getValue().tanggalKembaliProperty());
        cPetugas.setCellValueFactory(value -> value.getValue().petugasProperty());
        riwayatPinjam.setItems(peminjamList);
        
        userManager = UserManager.getInstance();
    }    
    
    public void setSelectedSurat(Surat surat) {
        selectedSurat = surat;
        fetchSurat();
    }
    
    public void setHomeController(HomeController controller) {
        this.homeController = controller;
    }
    
    private void fetchSurat() {
        // Data surat
        namaInstansi.setText(selectedSurat.getNamaInstansi());
        alamat.setText(selectedSurat.getAlamat());
        kota.setText(selectedSurat.getKota());
        nomorSurat.setText(selectedSurat.getNomorSurat());
        perihal.setText(selectedSurat.getPerihal());
        jenisSurat.getSelectionModel().select(selectedSurat.getJenisSurat());
        unit.getSelectionModel().select(selectedSurat.getUnit());
        tanggalSurat.setValue(DateTimeUtils.parseDateOnly(selectedSurat.getTanggalSurat()));
        tanggalSimpan.setValue(DateTimeUtils.parseDateOnly(selectedSurat.getTanggalSimpan()));
        brsr.getSelectionModel().select(selectedSurat.getKerahasiaan());
        sistem.getSelectionModel().select(selectedSurat.getSistem());
        index.setText(selectedSurat.getIndex());
        if (selectedSurat.getKode().contains("/")) {            
            kode.setText(selectedSurat.getKode().split("/")[0]);
            kode1.setText(selectedSurat.getKode().split("/")[1]);
            kode11.setText(selectedSurat.getKode().split("/")[2]);
        } else {
            kode.setText(selectedSurat.getKode());
            kode1.clear(); kode11.clear();
            kode1.setDisable(true); kode11.setDisable(true);
        }
        nomorUrut.setText(selectedSurat.getNomorUrut());
        lampiran.setText(selectedSurat.getLampiran());
        isiRingkasan.setText(selectedSurat.getRingkasan());
        catatan.setText(selectedSurat.getCatatan());
        
        attachments.clear();
        attachments.addAll(selectedSurat.getAttachments());
        listAtt.clear();
        attachments.forEach((a) -> { listAtt.add(a.getNamaFile()); });
        
        formPengembalian.setVisible(false);

        // Data Peminjam
        if (selectedSurat.isDipinjam()) {
            statusSurat.setVisible(true);
            selectedPeminjam = selectedSurat.getPeminjam();
            namaPeminjam.setText(selectedPeminjam.getNamaPeminjam());
            statusArsip.setText(selectedPeminjam.getStatus());
            jabatan.setText(selectedPeminjam.getJabatan());
            petugas.setText(selectedPeminjam.getPetugas());
            tanggalPinjam.setText(selectedPeminjam.getTanggalPinjam());
            tanggalKembali.setText(selectedPeminjam.getTanggalKembali());
        } else {
            statusSurat.setVisible(false);
            namaPeminjam.clear();
            statusArsip.setText("Tersedia");
            jabatan.clear();
            petugas.clear();
            tanggalPinjam.clear();
            tanggalKembali.clear();
        }
        
        peminjamManager.getPeminjamListById(
                selectedSurat.getUid(), 
                new ICallback<List<Peminjam>>() {
                    @Override
                    public void onSuccess(List<Peminjam> data) {
                        peminjamList.clear();
                        peminjamList.addAll(data);
                    }

                    @Override
                    public void onError(String message) {
                        DialogBuilder.showError(message);
                    }
                }
        );
    }

    @FXML
    private void printKk(MouseEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/e/arsip/view/PrintPreviewKk.fxml")
        );
        
        Parent root = loader.load();
        PrintPreviewKkController controller = (PrintPreviewKkController) loader.getController();
        controller.setSurat(selectedSurat);
        
        if (homeController != null) homeController.showPage(root);
    }

    @FXML
    private void onSistemSimpanChanged(ActionEvent event) {
        int i = sistem.getSelectionModel().getSelectedIndex();
        kode1.setDisable(i==0);
        kode11.setDisable(i==0);
    }
}
