/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.model.Surat;
import e.arsip.util.DateTimeUtils;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class PrintPreviewKkController implements Initializable {

    @FXML
    private Text indeks;
    @FXML
    private Text kode;
    @FXML
    private Text tanggal;
    @FXML
    private Text noUrut;
    @FXML
    private Text hal;
    @FXML
    private Text isiRingkasan;
    @FXML
    private Text lampiran;
    @FXML
    private Text dariKepada;
    @FXML
    private Text tanggalSurat;
    @FXML
    private Text noSurat;
    @FXML
    private Text pengelola;
    @FXML
    private Text paraf;
    @FXML
    private Text catatan;
    @FXML
    private Text indeks1;
    @FXML
    private Text kode1;
    @FXML
    private Text tanggal1;
    @FXML
    private Text noUrut1;
    @FXML
    private Text hal1;
    @FXML
    private Text isiRingkasan1;
    @FXML
    private Text lampiran1;
    @FXML
    private Text dariKepada1;
    @FXML
    private Text tanggalSurat1;
    @FXML
    private Text noSurat1;
    @FXML
    private Text pengelola1;
    @FXML
    private Text paraf1;
    @FXML
    private Text catatan1;
    @FXML
    private Text indeks11;
    @FXML
    private Text kode11;
    @FXML
    private Text tanggal11;
    @FXML
    private Text noUrut11;
    @FXML
    private Text hal11;
    @FXML
    private Text isiRingkasan11;
    @FXML
    private Text lampiran11;
    @FXML
    private Text dariKepada11;
    @FXML
    private Text tanggalSurat11;
    @FXML
    private Text noSurat11;
    @FXML
    private Text pengelola11;
    @FXML
    private Text paraf11;
    @FXML
    private Text catatan11;
    @FXML
    private VBox printLayout;

    /**
     * Initializes the controller class.
     */

    @FXML
    private void print(MouseEvent event) {
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(printLayout.getScene().getWindow())){
            boolean success = job.printPage(printLayout);
            if (success) {
                job.endJob();
            }
        }
    }
    
    public void setSurat(Surat surat) {
        indeks.setText(surat.getIndex());
        kode.setText(surat.getKode());
        tanggal.setText(DateTimeUtils.format(LocalDate.now()));
        noUrut.setText(surat.getNomorUrut());
        hal.setText(surat.getPerihal());
        isiRingkasan.setText(surat.getRingkasan());
        lampiran.setText(surat.getLampiran());
        dariKepada.setText(surat.getNamaInstansi());
        tanggalSurat.setText(surat.getTanggalSurat());
        noSurat.setText(surat.getNomorSurat());
        pengelola.setText(surat.getPetugas());
        catatan.setText(surat.getCatatan());
        
        indeks1.setText(surat.getIndex());
        kode1.setText(surat.getKode());
        tanggal1.setText(DateTimeUtils.format(LocalDate.now()));
        noUrut1.setText(surat.getNomorUrut());
        hal1.setText(surat.getPerihal());
        isiRingkasan1.setText(surat.getRingkasan());
        lampiran1.setText(surat.getLampiran());
        dariKepada1.setText(surat.getNamaInstansi());
        tanggalSurat1.setText(surat.getTanggalSurat());
        noSurat1.setText(surat.getNomorSurat());
        pengelola1.setText(surat.getPetugas());
        catatan1.setText(surat.getCatatan());
        
        indeks11.setText(surat.getIndex());
        kode11.setText(surat.getKode());
        tanggal11.setText(DateTimeUtils.format(LocalDate.now()));
        noUrut11.setText(surat.getNomorUrut());
        hal11.setText(surat.getPerihal());
        isiRingkasan11.setText(surat.getRingkasan());
        lampiran11.setText(surat.getLampiran());
        dariKepada11.setText(surat.getNamaInstansi());
        tanggalSurat11.setText(surat.getTanggalSurat());
        noSurat11.setText(surat.getNomorSurat());
        pengelola11.setText(surat.getPetugas());
        catatan11.setText(surat.getCatatan());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO        
    }
}
