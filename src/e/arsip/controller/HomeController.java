/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.UserManager;
import e.arsip.data.model.User;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class HomeController implements Initializable {

    @FXML
    private ImageView fotoProfile;
    @FXML
    private Text userName;
    
    private UserManager userManager;
    @FXML
    private VBox navHome;
    @FXML
    private VBox menuHome;
    @FXML
    private BorderPane container;
    @FXML
    private VBox navOther;
    @FXML
    private ImageView fotoProfile1;
    @FXML
    private Text userName1;
    

    /**
     * Initializes the controller class.
     */

    @FXML
    void btn_home(MouseEvent event)throws IOException {
        menuHome.setVisible(true);
        navHome.setVisible(true);
    }

    @FXML
    void btn_pengaturan(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pengaturan.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    private void btn_tentang(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Tentang.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }
    
    @FXML
    void btn_logout(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    @FXML
    void btn_input(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Input.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    void btn_manajemen(MouseEvent event)throws IOException {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/e/arsip/view/Manajemen.fxml")
        );
        Parent root = loader.load();
        ManajemenController controller = (ManajemenController) loader.getController();
        controller.setHomeController(this);
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    void btn_rekap(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Rekap.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }
    
    @FXML
    void btn_bukuagenda(MouseEvent event)throws IOException {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/e/arsip/view/Bukuagenda.fxml")
        );
        Parent root = loader.load();
        BukuagendaController controller = (BukuagendaController) loader.getController();
        controller.setHomeController(this);
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    void btn_peminjaman(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Peminjaman.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    void btn_pencarian(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pencarian.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    void btn_jra(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Jra.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }

    @FXML
    void btn_statistik(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Statistik.fxml"));
        container.setCenter(root);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }
    
    public void showPage(Parent page) {
        container.setCenter(page);
        menuHome.setVisible(false);
        navHome.setVisible(false);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
        User user = userManager.getActiveUser();
        userName.setText(user.getFullName());
        userName1.setText(user.getFullName());
        if (user.getImagePath() != null) {
            File profile = new File(user.getImagePath());
            fotoProfile.setImage(
                    new Image(profile.toURI().toString(), 
                    fotoProfile.getFitWidth(),
                    fotoProfile.getFitHeight(),
                    false, false
            ));
            fotoProfile1.setImage(
                    new Image(profile.toURI().toString(), 
                    fotoProfile.getFitWidth(),
                    fotoProfile.getFitHeight(),
                    false, false
            ));
        }        
    }
}
