/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.PeminjamManager;
import e.arsip.data.manager.SuratManager;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Peminjam;
import e.arsip.data.model.Surat;
import e.arsip.data.model.User;
import e.arsip.util.DateTimeUtils;
import e.arsip.util.DialogBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class StatistikController implements Initializable {

    private ImageView fotoProfile;
    private Text userName;    
    @FXML
    private BarChart<String, Number> barUnit;
    @FXML
    private BarChart<String, Number> barPeminjaman;
    @FXML
    private PieChart pieJenis;
    @FXML
    private PieChart piePetugas;
    
    private UserManager userManager;
    private SuratManager suratManager;
    private PeminjamManager peminjamManager;
    private ObservableList<Surat> suratList;
    private ObservableList<Peminjam> peminjamList;
    private ObservableList<PieChart.Data> pieDataJenis, pieDataPetugas;
    private ObservableList<XYChart.Series<String, Number>> barDataUnit, barDataPeminjaman;
    
    /**
     * Initializes the controller class.
     */
    
    void btn_home(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Home.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pengaturan(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pengaturan.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    private void btn_tentang(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Tentang.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_logout(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_input(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Input.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_manajemen(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Manajemen.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_rekap(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Rekap.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_bukuagenda(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Bukuagenda.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_peminjaman(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Peminjaman.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pencarian(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pencarian.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_jra(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Jra.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_statistik(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Statistik.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
        
        suratList = FXCollections.observableArrayList();
        suratManager = SuratManager.getInstance();
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
                prepareSuratChart();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        peminjamList = FXCollections.observableArrayList();
        peminjamManager = PeminjamManager.getInstance();
        peminjamManager.getPeminjamList(new ICallback<List<Peminjam>>() {
            @Override
            public void onSuccess(List<Peminjam> data) {
                peminjamList.clear();
                peminjamList.addAll(data);
                preparePeminjamanChart();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }            
        });
        
        pieJenis.setData(pieDataJenis);
        piePetugas.setData(pieDataPetugas);
        barUnit.setData(barDataUnit);
        barPeminjaman.setData(barDataPeminjaman);
    }    
    
    private void prepareSuratChart() {
        //Statistik Surat
        pieDataJenis = FXCollections.observableArrayList();
        pieDataPetugas = FXCollections.observableArrayList();
        barDataUnit = FXCollections.observableArrayList();
        int countM = 0, countK = 0,
                countU1 = 0, countU2 = 0, countU3 = 0, 
                countU4 = 0, countU5 = 0;
        HashMap<String, Integer> mapPetugas = new HashMap<>();
        XYChart.Series<String, Number> dataUnit = new XYChart.Series<>();
        dataUnit.setName("Unit");
        for (Surat s : suratList) {
            if (s.getJenisSurat().equalsIgnoreCase("Masuk")) countM++;
            else if (s.getJenisSurat().equalsIgnoreCase("Keluar")) countK++;
            
            if (mapPetugas.containsKey(s.getPetugas())) {
                int temp = mapPetugas.get(s.getPetugas());
                mapPetugas.put(s.getPetugas(), ++temp);
            } else {
                mapPetugas.put(s.getPetugas(), 1);
            }
            
            switch (s.getUnit()) {
                case "Kepegawaian":
                    countU1++;
                    break;
                case "Umum":
                    countU2++;
                    break;
                case "Keuangan":
                    countU3++;
                    break;
                case "Kemahasiswaan":
                    countU4++;
                    break;
                case "Akademi":
                    countU5++;
                    break;
            }
        }
        pieDataJenis.add(new PieChart.Data("Surat Masuk", countM));
        pieDataJenis.add(new PieChart.Data("Surat Keluar", countK));
        
        for (String p : mapPetugas.keySet()) {
            pieDataPetugas.add(new PieChart.Data(p, mapPetugas.get(p)));
        }      
        
        dataUnit.getData().add(new XYChart.Data<>("Kepegawaian", countU1));
        dataUnit.getData().add(new XYChart.Data<>("Umum", countU2));
        dataUnit.getData().add(new XYChart.Data<>("Keuangan", countU3));
        dataUnit.getData().add(new XYChart.Data<>("Kemahasiswaan", countU4));
        dataUnit.getData().add(new XYChart.Data<>("Akademi", countU5));
        barDataUnit.add(dataUnit);        
    }
    
    private void preparePeminjamanChart() {     
        barDataPeminjaman = FXCollections.observableArrayList();
        XYChart.Series<String, Number> dataPetugas = new XYChart.Series<>();
        HashMap<String, Integer> mapPeminjaman = new HashMap<>();       
        dataPetugas.setName("Bulan");
        for (Peminjam p : peminjamList) {
            String month = DateTimeUtils.formatMonthOnly(
                    DateTimeUtils.parseDateOnly(p.getTanggalPinjam())
            );
            if (mapPeminjaman.containsKey(month)) {
                System.out.println(month);
                int temp = mapPeminjaman.get(month);
                mapPeminjaman.put(month, ++temp);
            } else {
                mapPeminjaman.put(month, 1);
                System.out.println(month);
            }
        }
        
        for (String p : mapPeminjaman.keySet()) {
           dataPetugas.getData().add(new XYChart.Data<>(p, mapPeminjaman.get(p)));
        }  
        barDataPeminjaman.add(dataPetugas);
    }
}
