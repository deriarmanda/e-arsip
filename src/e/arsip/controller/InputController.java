/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.SuratManager;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Attachment;
import e.arsip.data.model.Surat;
import e.arsip.data.model.Unit;
import e.arsip.data.model.User;
import e.arsip.util.DateTimeUtils;
import e.arsip.util.DialogBuilder;
import e.arsip.util.FileUtils;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class InputController implements Initializable {

    @FXML
    private TextField namaInstansi;
    @FXML
    private TextField alamat;
    @FXML
    private TextField kota;
    @FXML
    private TextField nomorSurat;
    @FXML
    private TextField perihal;
    @FXML
    private DatePicker tanggalSurat;
    @FXML
    private DatePicker tanggalSimpan;
    @FXML
    private ComboBox<String> unit;
    @FXML
    private ComboBox<String> brsr;
    @FXML
    private ComboBox<String> sistem;
    @FXML
    private TextField index;
    @FXML
    private TextField kode;
    @FXML
    private TextField nomorUrut;
    @FXML
    private TextField lampiran;
    @FXML
    private TextArea catatan;
    @FXML
    private TextArea isiRingkasan;
    @FXML
    private Button btnTambah;
    @FXML
    private Button btnHapus;
    @FXML
    private ListView listAttachment;
    @FXML
    private ComboBox<String> jenisSurat;
    private ImageView fotoProfile;
    private Text userName;
    
    private SuratManager suratManager;
    private UserManager userManager;
    private Surat surat;
    private ArrayList<Attachment> listAtt;
    private ObservableList<String> attachments;
    @FXML
    private TextField kode1;
    @FXML
    private TextField kode11;
    
    /**
     * Initializes the controller class.
     */
    
    void btn_home(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Home.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pengaturan(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pengaturan.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    private void btn_tentang(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Tentang.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_logout(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_input(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Input.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_manajemen(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Manajemen.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_rekap(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Rekap.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_bukuagenda(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Bukuagenda.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_peminjaman(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Peminjaman.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pencarian(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pencarian.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_jra(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Jra.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_statistik(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Statistik.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }        

    @FXML
    void btnsimpanSurat(MouseEvent event) {
        //simpan ke database
        surat.setNamaInstansi(namaInstansi.getText());
        surat.setAlamat(alamat.getText());
        surat.setKota(kota.getText());
        surat.setNomorSurat(nomorSurat.getText());
        surat.setPerihal(perihal.getText());
        surat.setTanggalSurat(DateTimeUtils.format(tanggalSurat.getValue() == null? LocalDate.now() : tanggalSurat.getValue()));
        surat.setTanggalSimpan(DateTimeUtils.format(tanggalSimpan.getValue() == null? LocalDate.now() : tanggalSimpan.getValue()));
        surat.setJenisSurat(jenisSurat.getValue());
        surat.setUnit(unit.getValue());
        surat.setKerahasiaan(brsr.getValue());
        surat.setSistem(sistem.getValue());
        surat.setIndex(index.getText());
        surat.setKode(kode.getText()+(kode1.isDisable()? "" : "/"+kode1.getText()+"/"+kode11.getText()));
        surat.setNomorUrut(nomorUrut.getText());
        surat.setLampiran(lampiran.getText());
        surat.setRingkasan(isiRingkasan.getText());
        surat.setCatatan(catatan.getText());   
        surat.setPetugas(userManager.getActiveUser().getFullName());
        surat.setAttachments(listAtt);
        
        suratManager.insertSurat(surat, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                DialogBuilder.showInfo(data);
                onSaveSuratSuccess();                
            }

            @Override
            public void onError(String message) {
                onSaveSuratError(message);
            }
            
        });
    }
    
    @FXML
    public void ButtonTambah(ActionEvent event){
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        
        File seletedFile = FileUtils.openFileDialog(stage);
        
        if(seletedFile != null) {            
            Attachment att = new Attachment();
            att.setNamaFile(seletedFile.getName());
            att.setPathFile(seletedFile.getAbsolutePath());
            listAtt.add(att);
                    
            attachments.add(att.getNamaFile());
        } else {
            System.out.println("File Tidak Valid");
        }
        
    }
    
    public void buttonHapus(ActionEvent event){
        String selected = listAttachment.getSelectionModel().getSelectedItem().toString();
        if (selected != null) {
            for (Attachment att : listAtt) {
                if (att.getNamaFile().equals(selected)) {
                    listAtt.remove(att);
                    attachments.remove(selected);
                    break;
                }
            }
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        suratManager = SuratManager.getInstance();
        userManager = UserManager.getInstance();
        
        tanggalSurat.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                if (object == null) return "";
                else return DateTimeUtils.formatDatePicker(object);
            }

            @Override
            public LocalDate fromString(String string) {
                LocalDate date = DateTimeUtils.parseDatePicker(string);
                return (date == null? LocalDate.now() : date);
            }
        });
        tanggalSimpan.setValue(LocalDate.now());
        
        userManager.getUnit(new ICallback<List<Unit>>() {
            @Override
            public void onSuccess(List<Unit> data) {
                unit.getItems().clear();
                for (Unit u : data) {
                    unit.getItems().add(u.getNama());
                }
                unit.getSelectionModel().selectFirst();
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        jenisSurat.getItems().add("Masuk");
        jenisSurat.getItems().add("Keluar");
        jenisSurat.getSelectionModel().selectFirst();
        
        brsr.getItems().add("Biasa");
        brsr.getItems().add("Rahasia");
        brsr.getItems().add("Sangat Rahasia");
        brsr.getSelectionModel().selectFirst();
        
        sistem.getItems().add("Abjad");
        sistem.getItems().add("Tanggal Surat");
//        sistem.getItems().add("Desimal");
//        sistem.getItems().add("Terminal Digit");
//        sistem.getItems().add("Wilayah");
        sistem.getSelectionModel().selectFirst();
        
        surat = new Surat();
        listAtt = new ArrayList();
        attachments = FXCollections.observableArrayList();
        listAttachment.setItems(attachments);
        listAttachment.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    int index = listAttachment.getSelectionModel().getSelectedIndex();
                    if (index >= 0 && index < listAtt.size()) {
                        Attachment selected = listAtt.get(index);
                        try {
                            Desktop.getDesktop().open(new File(selected.getPathFile()));
                        } catch (IOException ex) {
                            DialogBuilder.showError("Gagal membuka file yg dipilih.\nEx: "+ex.getMessage());
                        }
                    }
                }
            }
        });
    }    
    
    private void onSaveSuratSuccess() {
        namaInstansi.clear();
        alamat.clear();
        kota.clear();
        nomorSurat.clear();
        perihal.clear();
        tanggalSurat.setValue(null);
        tanggalSimpan.setValue(LocalDate.now());
        jenisSurat.getSelectionModel().selectFirst();
        unit.getSelectionModel().selectFirst();
        brsr.getSelectionModel().selectFirst();
        sistem.getSelectionModel().selectFirst();
        index.clear();
        kode.clear(); kode1.clear(); kode11.clear();
        nomorUrut.clear();
        lampiran.clear();
        isiRingkasan.clear();
        catatan.clear();
        
        listAtt.clear();
        attachments.clear();
        surat = new Surat();
    }
    
    private void onSaveSuratError(String error) {
        DialogBuilder.showError(error);
    }

    @FXML
    private void onSistemSimpanChanged(ActionEvent event) {
        int i = sistem.getSelectionModel().getSelectedIndex();
        kode1.setDisable(i==0);
        kode11.setDisable(i==0);
    }
}
