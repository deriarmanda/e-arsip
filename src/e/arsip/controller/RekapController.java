/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e.arsip.controller;

import e.arsip.data.manager.ICallback;
import e.arsip.data.manager.SuratManager;
import e.arsip.data.manager.UserManager;
import e.arsip.data.model.Surat;
import e.arsip.data.model.User;
import e.arsip.util.DialogBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kekek Leliyana
 */
public class RekapController implements Initializable {

    @FXML
    private TableView<Surat> rekapSurat;
    @FXML
    private TableColumn<Surat, Number> no;
    @FXML
    private TableColumn<Surat, String> mk;
    @FXML
    private TableColumn<Surat, String> nomorSurat;
    @FXML
    private TableColumn<Surat, String> tanggalSurat;
    @FXML
    private TableColumn<Surat, String> kepada;
    @FXML
    private TableColumn<Surat, String> hal;
    @FXML
    private TableColumn<Surat, String> kode;
    private ImageView fotoProfile;
    private Text userName;
    @FXML
    private ComboBox<String> filterJenisSurat;
    
    private UserManager userManager;
    private SuratManager suratManager;
    private ObservableList<Surat> suratList, rekapList;
    private Surat selectedSurat;

    /**
     * Initializes the controller class.
     */
    
    void btn_home(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Home.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pengaturan(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pengaturan.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    private void btn_tentang(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Tentang.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_logout(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Login.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_input(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Input.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_manajemen(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Manajemen.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_rekap(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Rekap.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }
    
    void btn_bukuagenda(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Bukuagenda.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_peminjaman(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Peminjaman.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_pencarian(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Pencarian.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_jra(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Jra.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }

    void btn_statistik(MouseEvent event)throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/e/arsip/view/Statistik.fxml"));

        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();

        stage.setScene(new Scene(root));
    }    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        userManager = UserManager.getInstance();
        
        suratList = FXCollections.observableArrayList();
        rekapList = FXCollections.observableArrayList();
        
        selectedSurat = null;
        no.setCellValueFactory(value -> new SimpleIntegerProperty(rekapList.indexOf(value.getValue())+1));
        mk.setCellValueFactory(value -> value.getValue().jenisSuratProperty());
        nomorSurat.setCellValueFactory(value -> value.getValue().nomorSuratProperty());
        kode.setCellValueFactory(value -> value.getValue().kodeProperty());
        hal.setCellValueFactory(value -> value.getValue().perihalProperty());
        tanggalSurat.setCellValueFactory(value -> value.getValue().tanggalSuratProperty());
        kepada.setCellValueFactory(value -> value.getValue().namaInstansiProperty());
        rekapSurat.setItems(rekapList);
        rekapSurat.getSelectionModel().selectedItemProperty().addListener((
                ObservableValue<? extends Surat> observable, 
                Surat oldValue, 
                Surat newValue) -> {
                    selectedSurat = observable.getValue();
        });
        
        suratManager = SuratManager.getInstance();
        suratManager.getSuratList(true, new ICallback<ObservableList<Surat>>() {
            @Override
            public void onSuccess(ObservableList<Surat> data) {
                suratList.clear();
                suratList.addAll(data);
            }

            @Override
            public void onError(String message) {
                DialogBuilder.showError(message);
            }
            
        });
        
        filterJenisSurat.getItems().add("Surat Masuk");
        filterJenisSurat.getItems().add("Surat Keluar");
        filterJenisSurat.valueProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String filter = newValue.substring(6, newValue.length());
            rekapList.clear();
            for (Surat surat : suratList) {
                if (surat.getJenisSurat().equals(filter)) rekapList.add(surat);
            }
        });
    }

    @FXML
    private void showDetail(ActionEvent event) {
        if (selectedSurat != null) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/e/arsip/view/Manajemen.fxml"));
                Parent root = loader.load();
                ManajemenController controller = (ManajemenController) loader.getController();
                Node node = (Node) event.getSource();
                Stage stage = (Stage) node.getScene().getWindow();
                controller.setSelectedSurat(selectedSurat);
                stage.setScene(new Scene(root));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
}
